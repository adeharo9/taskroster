package com.adeharo.models

import com.adeharo.auxiliary.ZonedDateTimeSerializer
import java.time.ZonedDateTime
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.`java-time`.*

object Tasks: Table("tasks") {
    val id = integer("id").autoIncrement()
    val user_id = integer("user_id").references(Users.id).nullable()
    val description = text("description").nullable()
    val completed = bool("completed").default(false)
    val created_at = datetime("created_at").defaultExpression(CurrentDateTime())
    val updated_at = datetime("updated_at").defaultExpression(CurrentDateTime())
    val deleted_at = datetime("deleted_at").nullable()

    override val primaryKey = PrimaryKey(id)

}

@Serializable
data class Task (
    val id: Int,
    val user_id: Int?,
    val description: String?,
    val completed: Boolean,
    @Serializable(with=ZonedDateTimeSerializer::class)
    val created_at: ZonedDateTime,
    @Serializable(with=ZonedDateTimeSerializer::class)
    val updated_at: ZonedDateTime,
    @Serializable(with=ZonedDateTimeSerializer::class)
    val deleted_at: ZonedDateTime?,
)
