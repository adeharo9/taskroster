package com.adeharo.models

import com.adeharo.auxiliary.ZonedDateTimeSerializer
import java.time.ZonedDateTime
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.`java-time`.*

object Users: Table("users") {
    val id = integer("id").autoIncrement()
    val username = varchar("username", 255)
    val hash = varchar("hash", 64)
    val created_at = datetime("created_at").defaultExpression(CurrentDateTime())
    val updated_at = datetime("updated_at").defaultExpression(CurrentDateTime())
    val deleted_at = datetime("deleted_at").nullable()

    override val primaryKey = PrimaryKey(id)
}

@Serializable
data class User (
    val id: Int,
    val username: String,
    val hash: String,
    @Serializable(with=ZonedDateTimeSerializer::class)
    val created_at: ZonedDateTime,
    @Serializable(with=ZonedDateTimeSerializer::class)
    val updated_at: ZonedDateTime,
    @Serializable(with=ZonedDateTimeSerializer::class)
    val deleted_at: ZonedDateTime?
)
