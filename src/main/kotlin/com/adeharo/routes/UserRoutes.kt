package com.adeharo.routes

import com.adeharo.models.*
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.ZoneId
import java.time.ZonedDateTime

fun Route.userRouting() {
    route("/user") {
        get {
            val users = mutableListOf<User>()

            transaction {
                Users.selectAll().map {
                    users.add(
                        User(
                            id = it[Users.id],
                            username = it[Users.username],
                            hash = it[Users.hash],
                            created_at = ZonedDateTime.of(it[Users.created_at], ZoneId.of("Z")),
                            updated_at = ZonedDateTime.of(it[Users.updated_at], ZoneId.of("Z")),
                            deleted_at = if(it[Users.deleted_at] != null) ZonedDateTime.of(it[Users.deleted_at], ZoneId.of("Z")) else null
                        )
                    )
                }
            }

            call.respond(users)
        }
        get("{id}") {
            val id = call.parameters["id"]?.toInt() ?: return@get call.respondText(
                "Missing or malformed id",
                status = HttpStatusCode.BadRequest
            )
            var users = mutableListOf<User>()

            transaction {
                val user = Users.select { Users.id eq  id }.singleOrNull() ?: return@transaction

                users.add(
                    User(
                        id = user[Users.id],
                        username = user[Users.username],
                        hash = user[Users.hash],
                        created_at = ZonedDateTime.of(user[Users.created_at], ZoneId.of("Z")),
                        updated_at = ZonedDateTime.of(user[Users.updated_at], ZoneId.of("Z")),
                        deleted_at = if(user[Users.deleted_at] != null) ZonedDateTime.of(user[Users.deleted_at], ZoneId.of("Z")) else null
                    )
                )
            }

            call.respond(users)
        }
        post {

        }
        patch("{id}") {

        }
        delete("{id}") {

        }
    }
}
