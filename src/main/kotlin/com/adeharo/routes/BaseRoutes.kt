package com.adeharo.routes

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.baseRouting() {
    route("/") {
        get {
            call.respondText("BASE ENDPOINT", contentType = ContentType.Text.Plain)
        }
    }
}
