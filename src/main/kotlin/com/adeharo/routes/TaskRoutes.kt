package com.adeharo.routes

import com.adeharo.models.*
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.ZoneId
import java.time.ZonedDateTime

fun Route.taskRouting() {
    route("/task") {
        get {
            val tasks = mutableListOf<Task>()

            transaction {
                Tasks.selectAll().map {
                    tasks.add(
                        Task(
                            id = it[Tasks.id],
                            user_id = it[Tasks.user_id],
                            description = it[Tasks.description],
                            completed = it[Tasks.completed],
                            created_at = ZonedDateTime.of(it[Tasks.created_at], ZoneId.of("Z")),
                            updated_at = ZonedDateTime.of(it[Tasks.updated_at], ZoneId.of("Z")),
                            deleted_at = if(it[Tasks.deleted_at] != null) ZonedDateTime.of(it[Tasks.deleted_at], ZoneId.of("Z")) else null
                        )
                    )
                }
            }

            call.respond(tasks)
        }
        get("{id}") {

        }
        post {

        }
        patch("{id") {

        }
        delete("{id}") {

        }
    }
}
