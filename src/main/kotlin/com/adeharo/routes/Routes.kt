package com.adeharo.routes

import io.ktor.application.Application
import io.ktor.routing.routing

fun Application.registerRoutes() {
    routing {
        baseRouting()
        taskRouting()
        userRouting()
    }
}