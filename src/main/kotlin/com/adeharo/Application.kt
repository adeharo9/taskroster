package com.adeharo

import com.adeharo.routes.registerRoutes
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.serialization.*
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.module(testing: Boolean = false) {
    Flyway.configure().dataSource(
        System.getenv("DB_URL"),
        System.getenv("DB_USER"),
        System.getenv("DB_PASSWORD")
    ).load().migrate()

    Database.connect(
        System.getenv("DB_URL"),
        user = System.getenv("DB_USER"),
        password = System.getenv("DB_PASSWORD")
    )

    install(ContentNegotiation) {
        json()
    }

    registerRoutes()
}
