# [TaskRoster](https://gitlab.com/adeharo9/taskroster)

<img src="https://gitlab.com/adeharo9/taskroster/-/raw/master/taskroster.png" alt="taskroster" align="right"/>

![v0.1.0](https://img.shields.io/badge/version-v0.1.0-blue "v1.0.0-alpha")
![BSD 3-clause license](https://img.shields.io/badge/license-BSD%203--clause-green "BSD 3-clause license")

Simple task utility.

## Table of contents

1. [Docker](#docker)

## Docker

The Docker image building process already builds the sources, so there is no need for external building.

```shell
docker build -t taskroster . &&
docker run -p 8080:8080 taskroster
```
